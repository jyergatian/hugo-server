# HugoServer
A [Panic Nova](https://nova.app/) plug-in for running [Hugo](https://gohugo.io/) server, build, and clean.

Big thanks to [JekyllServe](https://github.com/dempseyatgithub/JekyllServe) for the inspiration and starting point.