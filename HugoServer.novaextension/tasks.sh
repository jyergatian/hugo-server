#!/bin/sh
# Map incoming environment variables to task arguments

if [ "$HUGOSERVER_DISABLE_FAST_RENDER" = "1" ] ; then
	DISABLE_FAST_RENDER_FLAG="--disableFastRender"
fi

if [ "$HUGOSERVER_NAVIGATE_TO_CHANGED" = "1" ] ; then
	NAVIGATE_TO_CHANGED_FLAG="--navigateToChanged"
fi

if [ "$HUGOSERVER_DRAFTS" = "1" ] ; then
	DRAFTS_FLAG="--buildDrafts"
fi

if [ "$HUGOSERVER_DRAFTS_BUILD" = "1" ] ; then
	BUILD_DRAFTS_FLAG="--buildDrafts"
fi

if [ "$HUGOSERVER_FUTURE" = "1" ] ; then
	FUTURE_FLAG="--buildFuture"
fi

if [ "$HUGOSERVER_FUTURE_BUILD" = "1" ] ; then
	BUILD_FUTURE_FLAG="--buildFuture"
fi

if [ "$HUGOSERVER_VERBOSE" = "1" ] ; then
	VERBOSE_FLAG="--verbose"
	echo "$VERBOSE_FLAG"
fi

if [ "$HUGOSERVER_VERBOSE_BUILD" = "1" ] ; then
	BUILD_VERBOSE_FLAG="--verbose"
	echo "$BUILD_VERBOSE_FLAG"
fi

if [ -n "$HUGOSERVER_HOST" ] ; then
	if [[ $HUGOSERVER_HOST = *" "* ]]; then
  		echo "WARNING: Hostname '"$HUGOSERVER_HOST"' contains spaces. Using localhost instead."
	else
		HOST_FLAG="--baseURL"
		HOST_VALUE=$HUGOSERVER_HOST
	fi
fi

if [ -n "$HUGOSERVER_PORT" ] ; then
	if [[ $HUGOSERVER_PORT = *" "* ]]; then
  		echo "WARNING: Port '"$HUGOSERVER_PORT"' contains spaces. Using port 4000 instead."
	else
		PORT_FLAG="--port"
		PORT_VALUE=$HUGOSERVER_PORT
	fi
fi


# Run the selected task

if [ "$NOVA_TASK_NAME" = "run" ] ; then
	hugo server $HOST_FLAG $HOST_VALUE $PORT_FLAG $PORT_VALUE $DISABLE_FAST_RENDER_FLAG $NAVIGATE_TO_CHANGED_FLAG $DRAFTS_FLAG $FUTURE_FLAG $VERBOSE_FLAG $HUGOSERVER_CUSTOM_ARGS
fi

if [ "$NOVA_TASK_NAME" = "build" ] ; then
	hugo $BUILD_DRAFTS_FLAG $BUILD_FUTURE_FLAG $BUILD_VERBOSE_FLAG $HUGOSERVER_CUSTOM_ARGS_BUILD
fi

if [ "$NOVA_TASK_NAME" = "clean" ] ; then
	hugo --cleanDestinationDir
fi