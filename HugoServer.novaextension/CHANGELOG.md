## Version 1.1

- Removed unnecessary options from Build task
- Separated Build and Run options for future, drafts, and verbose
- Separated Build and Run additional arguments

## Version 1.0

Initial release of HugoServer extension for Nova.

- Integrates Hugo server, build, and clean commands with Nova Tasks
- Supports configuration for common command options 
